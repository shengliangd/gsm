import xml.etree.cElementTree as ET


class GSM:
    def __init__(self, xml_input):
        """Take a xml as input, parse it and construct the state machine, then go to initial state."""
        # Parse using ET
        tree = ET.ElementTree(file=xml_input)
        root = tree.getroot()

        # State table
        self.states = dict()  # {'Idle' : {'Greeting' : ('Resp1', 'State1')}, ...}
        for e in root:
            state_id = e.attrib['id']
            nstate_id = e.attrib['next']
            pattern = e.find('pattern').text
            response = e.find('response').text
            self.states.setdefault(state_id, dict())[pattern] = (response, nstate_id)
        self.state = 'init'

    def transit(self, pattern: str):
        """Take a pattern, transit to next state and return a response.
            If there is no such pattern, throw KeyError."""
        (response, self.state) = self.states[self.state][pattern]
        return response
